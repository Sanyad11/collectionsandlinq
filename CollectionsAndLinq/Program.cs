﻿using CollectionsAndLinq.Services;
using System;

namespace CollectionsAndLinq
{
    class Program
    {
        static void Main(string[] args)
        {
            ProjectService projectService = new ProjectService();
            
            bool isNotEnd = true;
                        int userId;
            while (isNotEnd)
            {
                WriteMenu();

                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Enter user id");
                        if (int.TryParse(Console.ReadLine(), out userId)){
                            var res1 = projectService.GetTasksCountByUserId(userId);
                            foreach (var entry in res1.Result)
                            {
                                Console.WriteLine($"{entry.Key} - {entry.Value}");
                            }
                        }
                        break;
                    case "2":
                        Console.WriteLine("Enter user id");
                        if (int.TryParse(Console.ReadLine(), out userId))
                        {
                            var res2 = projectService.GetTasksWithLimitedName(userId, 45);
                            foreach (var entry in res2.Result)
                            {
                                Console.WriteLine(entry);
                            }
                        }
                        break;
                    case "3":
                        Console.WriteLine("Enter user id");
                        if (int.TryParse(Console.ReadLine(), out userId))
                        {
                            var res3 = projectService.GetTasksEndedOnYear(userId, 2020);
                            foreach (var entry in res3.Result)
                            {
                                Console.WriteLine(entry);
                            }
                        }
                        break;
                    case "4":
                        var res4 = projectService.GetTeamsWithWorkers();
                        foreach (var entry in res4.Result)
                        {
                            Console.WriteLine(entry);
                        }
                        break;
                    case "5":
                        var res5 = projectService.GetUsersWithTasks();
                        foreach (var entry in res5.Result)
                        {
                            Console.WriteLine(entry);
                        }
                        break;
                    case "6":
                        Console.WriteLine("Enter user id");
                        if (int.TryParse(Console.ReadLine(), out userId))
                        {
                            Console.WriteLine(projectService.GetUsersWithProjectAndTasks(userId).Result);
                            
                        }
                        break;
                    case "7":
                        var res7 = projectService.GetProjectAndTasks();
                        foreach (var entry in res7.Result)
                        {
                            Console.WriteLine(entry);
                        }
                        break;
                    case "0":
                        isNotEnd = false;
                        break;
                    default:
                        Console.WriteLine("Wrong char");
                        break;
                }
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void WriteMenu()
        {
            Console.WriteLine("Menu:");
            Console.WriteLine("1: GetTasksCountByUserId");
            Console.WriteLine("2: GetTasksWithLimitedName");
            Console.WriteLine("3: GetTasksEndedOnYear");
            Console.WriteLine("4: GetTeamsWithWorkers");
            Console.WriteLine("5: GetUsersWithTasks");
            Console.WriteLine("6: GetUsersWithProjectAndTasks");
            Console.WriteLine("7: GetProjectAndTasks");
            Console.WriteLine("0: Close cmd");
        }
    }
}
