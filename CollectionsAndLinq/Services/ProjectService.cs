﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using CollectionsAndLinq.DTO;
using Newtonsoft.Json;

namespace CollectionsAndLinq.Services
{
    public class ProjectService
    {
        private string baseUrl = "https://bsa20.azurewebsites.net/api/";
        private HttpClient httpClient = new HttpClient();
        public async Task<Dictionary<ProjectDTO, int>> GetTasksCountByUserId(int userId)
        {
            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var projects = httpClient.GetAsync($"{baseUrl}Projects");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());
            var deserializedProjects = JsonConvert.DeserializeObject<List<ProjectDTO>>(await tasks.Result.Content.ReadAsStringAsync());
            return deserializedTasks
                .Where(task => task.PerformerId == userId)
                .Join(deserializedProjects,
                task => task.ProjectId,
                project => project.Id,
                (task, project) => project)
                .GroupBy(task => task)
                .ToDictionary(group => group.Key, group => group.Count());
        }
        public async Task<IEnumerable<TaskDTO>> GetTasksWithLimitedName(int userId, int limit)
        {
            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());
            return deserializedTasks
                .Where(task => task.PerformerId == userId && task.Name.Length < limit);
        }
        public async Task<IEnumerable<SimpleTaskDTO>> GetTasksEndedOnYear(int userId, int year)
        {
            var states = httpClient.GetAsync($"{baseUrl}TaskStates");
            var deserializedStates = JsonConvert.DeserializeObject<List<TaskStateModelDTO>>(await states.Result.Content.ReadAsStringAsync());
            int finishedStateId = deserializedStates.First(state => state.Value == "Finished").Id;

            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());

            return deserializedTasks
                .Where(task => task.PerformerId == userId && task.State == finishedStateId && task.FinishedAt != null)
                .Where(task => task.FinishedAt.Year == year)
                .Select(task => new SimpleTaskDTO() { Id = task.Id, Name = task.Name });
        }

        public async Task<IEnumerable<TeamWithWorkersDTO>> GetTeamsWithWorkers()
        {
            var teams = httpClient.GetAsync($"{baseUrl}Teams");
            var deserializedTeams = JsonConvert.DeserializeObject<List<TeamDTO>>(await teams.Result.Content.ReadAsStringAsync());

            var users = httpClient.GetAsync($"{baseUrl}Users");
            var deserializedUsers = JsonConvert.DeserializeObject<List<UserDTO>>(await users.Result.Content.ReadAsStringAsync());

            IEnumerable<TeamWithWorkersDTO> teamsWithWorkers = deserializedTeams
                .Select(team => new TeamWithWorkersDTO() { Id = team.Id, Name = team.Name });
            return deserializedTeams
                .Join(
                    deserializedUsers
                    .Where(user => DateTime.Now.Year - user.Birthday.Year > 10)
                    .OrderByDescending(user => user.RegisteredAt)
                    .GroupBy(user => user.TeamId),
                    team => team.Id,
                    userGroup => userGroup.Key,
                    (team, userGroup) => new TeamWithWorkersDTO()
                    { Id = team.Id, Name = team.Name, Workers = userGroup.ToList() }
                )
                .Concat(teamsWithWorkers)
                .GroupBy(team => team.Id)
                .Select(group => group.First());

        }

        public async Task<IEnumerable<UserWithTasksDTO>> GetUsersWithTasks()
        {
            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());

            var users = httpClient.GetAsync($"{baseUrl}Users");
            var deserializedUsers = JsonConvert.DeserializeObject<List<UserDTO>>(await users.Result.Content.ReadAsStringAsync());

            IEnumerable<UserWithTasksDTO> usersWithTasks = deserializedUsers.Select(user => new UserWithTasksDTO()
            {
                Birthday = user.Birthday,
                Email = user.Email,
                FirstName = user.FirstName,
                Id = user.Id,
                LastName = user.LastName,
                RegisteredAt = user.RegisteredAt,
                TeamId = user.TeamId
            });
            return deserializedTasks
                 .OrderByDescending(task => task.Name.Length)
                 .GroupBy(task => task.PerformerId)
                 .Join(
                    usersWithTasks,
                    group => group.Key,
                    user => user.Id,
                    (group, user) =>
                    {
                        user.Tasks = group.ToList();
                        return user;
                    })
                 .Concat(usersWithTasks)
                 .GroupBy(user => user.Id)
                 .Select(group => group.First());
        }
        public async Task<UserWithProjectAndTasksDTO> GetUsersWithProjectAndTasks(int userId)
        {
            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());

            var user = httpClient.GetAsync($"{baseUrl}Users/{userId}");
            var deserializedUser = JsonConvert.DeserializeObject<UserDTO>(await user.Result.Content.ReadAsStringAsync());

            var projects = httpClient.GetAsync($"{baseUrl}Projects");
            var deserializedProjects = JsonConvert.DeserializeObject<List<ProjectDTO>>(await tasks.Result.Content.ReadAsStringAsync());

            var states = httpClient.GetAsync($"{baseUrl}TaskStates");
            var deserializedStates = JsonConvert.DeserializeObject<List<TaskStateModelDTO>>(await states.Result.Content.ReadAsStringAsync());
            int finishedStateId = deserializedStates.First(state => state.Value == "Finished").Id;

            IOrderedEnumerable<TaskDTO> userTasks = deserializedTasks
                .Where(task => task.PerformerId == userId)
                .OrderByDescending(task => task.CreatedAt);
            int projectId = userTasks.First().ProjectId;

            return new UserWithProjectAndTasksDTO()
            {
                User = deserializedUser,
                Project = deserializedProjects
                    .Where(proj => proj.Id == projectId)
                    .First(),
                LastProjectTasks = userTasks
                    .Where(task => task.ProjectId == projectId),
                TheLongestTask = userTasks
                    .OrderByDescending(task => task.FinishedAt.Subtract(task.CreatedAt))
                    .First(),
                CanceledOrUncompletedTasks = userTasks
                    .Where(task => task.State != finishedStateId)
            };

        }
        public async Task<IEnumerable<ProjectWithTasks>> GetProjectAndTasks()
        {
            var tasks = httpClient.GetAsync($"{baseUrl}Tasks");
            var deserializedTasks = JsonConvert.DeserializeObject<List<TaskDTO>>(await tasks.Result.Content.ReadAsStringAsync());

            var users = httpClient.GetAsync($"{baseUrl}Users");
            var deserializedUsers = JsonConvert.DeserializeObject<List<UserDTO>>(await users.Result.Content.ReadAsStringAsync());

            var projects = httpClient.GetAsync($"{baseUrl}Projects");
            var deserializedProjects = JsonConvert.DeserializeObject<List<ProjectDTO>>(await tasks.Result.Content.ReadAsStringAsync());



            return deserializedProjects
                .Where(proj => proj.Description.Length > 20 ||
                        deserializedTasks.Where(task => task.ProjectId == proj.Id).Count() < 3)
                .Select(proj =>
                {
                    IOrderedEnumerable<TaskDTO> projTasks = deserializedTasks
                        .Where(task => task.ProjectId == proj.Id)
                        .OrderBy(task => task.Description);
                    ProjectWithTasks projectWithTasks = new ProjectWithTasks();
                    projectWithTasks.Project = proj;
                    if (projTasks.Count() > 0)
                    {
                        projectWithTasks.TaskByDescription = projTasks.First();
                        projectWithTasks.TaskByName = projTasks.OrderBy(task => task.Name).First();
                    }
                    projectWithTasks.UsersCount = deserializedUsers
                        .Where(user => user.TeamId == proj.TeamId)
                        .Count();
                    return projectWithTasks;
                });
        }

    }
}
