﻿using Newtonsoft.Json;

namespace CollectionsAndLinq.DTO
{
    public class ProjectWithTasks
    {
        public ProjectDTO Project { get; set; }
        public TaskDTO TaskByDescription { get; set; }
        public TaskDTO TaskByName { get; set; }
        public int UsersCount { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
