﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CollectionsAndLinq.DTO
{
    public class TeamWithWorkersDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDTO> Workers { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
