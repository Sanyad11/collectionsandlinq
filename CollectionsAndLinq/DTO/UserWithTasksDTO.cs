﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CollectionsAndLinq.DTO
{
    public class UserWithTasksDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public int? TeamId { get; set; }
        public IEnumerable<TaskDTO> Tasks { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
