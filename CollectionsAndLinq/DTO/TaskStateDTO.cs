﻿namespace CollectionsAndLinq.DTO
{
    public enum TaskStateDTO
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
