﻿using Newtonsoft.Json;

namespace CollectionsAndLinq.DTO
{
    public class SimpleTaskDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
