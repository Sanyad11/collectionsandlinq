﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace CollectionsAndLinq.DTO
{
    public class UserWithProjectAndTasksDTO
    {
        public UserDTO User { get; set; }

        public ProjectDTO Project { get; set; }
        public IEnumerable<TaskDTO> LastProjectTasks { get; set; }
        public IEnumerable<TaskDTO> CanceledOrUncompletedTasks { get; set; }
        public TaskDTO TheLongestTask { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}
