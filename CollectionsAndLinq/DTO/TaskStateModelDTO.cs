﻿namespace CollectionsAndLinq.DTO
{
    public class TaskStateModelDTO
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
